import React from "react";
import Usestate from "./Component/Usestate";
import UseReduce from "./Component/UseReduce";
import UseEffect from "./Component/UseEffect";
import UseRef from "./Component/UseRef";
import UseContext from "./Component/UseContext";
import UseImperativeHandle from "./Component/UseImperativeHandle";
import UseMemo from "./Component/UseMemo";
import UseCallback from "./Component/UseCallback";

function App() {
  return (
    <div>
      <Usestate />
      <UseReduce />
      <UseEffect />
      <h1>UseRef</h1>
      <UseRef />
      <h1>UseImperativeHandle</h1>
      <UseImperativeHandle />
      <h1> UseContext</h1>
      <UseContext />
      <h1> UseMemo</h1>
     <UseMemo/>
     <h1>Usecalback</h1>
     <UseCallback/>
    </div>
  );
}

export default App;
