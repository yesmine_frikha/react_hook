import React, { forwardRef, useImperativeHandle, useState } from "react";

const Button = forwardRef((props, ref) => {
  const [toggle, setToggle] = useState(false);

  useImperativeHandle(ref, () => ({
      alterToggle() {
      setToggle(!toggle); // replace onclick
    },
  }));
  return (
    <>
      <button>Button From Child</button>
      <h1>{toggle && <span>Toggle</span>}</h1>
    </>
  );
});

export default Button;