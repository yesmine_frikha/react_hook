import React, { useState, useReducer } from "react";
const reducer = (state, action) => {
  switch (action.type) {
    case "INCREMENT":
      return { count: state.count + 1, showText: state.showText };
    case "toggleShowText":
      return { count: state.count + 1, showText: !state.showText };
    default:
      return state;
  }
};
const UseReduce = () => {
  //   const [count, setCount] = useState(0);
  //   const [showText, setShowText] = useState(true);
  // val contient tout les state : object , dispatch : function to change the value of state
  const [state, dispatch] = useReducer(reducer, { count: 0, showText: true });
  return (
    <div>
      <h1>{state.count}</h1>
      <button
        onClick={() => {
          dispatch({ type: "INCREMENT" });
          dispatch({ type: "toggleShowText" });
          //   setCount(count + 1);
          //   setShowText(!showText);
        }}
      >
        Click Here
      </button>
      {state.showText && <p>This is True</p>}
    </div>
  );
};

export default UseReduce;
