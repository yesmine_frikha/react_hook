import React, { useState } from "react";

const Usestate = () => {
  //   let counter = 0;
  const [counter, setCounter] = useState(0);
  const [inputValue, setInterval] = useState("Pedro");
  const increment = () => {
    setCounter(counter + 1);
    // console.log(counter);
  };
  let onChange = (event) => {
    const newValue = event.target.value;
    setInterval(newValue);
  };
  return (
    <div>
      <h1> {counter}</h1>{" "}
      <button className="btn btn-primary" onClick={increment}>
        Increment
      </button>
      <div>
        <input placeholder="enter something..." onChange={onChange} />
        {inputValue}
      </div>
    </div>
  );
};

export default Usestate;
