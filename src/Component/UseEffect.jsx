import React, { useEffect ,useState} from "react";
import axios from "axios";
function UseEffect() {
  //called when the page render
  const [data, setData] = useState("");
  const [count,setCount] = useState(0)
  useEffect(() => {
    // console.log("hello world")
    axios
      .get("https://jsonplaceholder.typicode.com/comments")
      .then((response) => {
        // console.log(response.data);
        setData(response.data[0].email);
        console.log("API WAS CALLED");
      });
  },[count]);// use effect callaed when the variable changed 
  return <div>Hello World 
    <h1>{data}</h1>
    <h1>{count}</h1>
    <button onClick={()=>{setCount(count+1)}}>Clik</button>
    </div>;
}

export default UseEffect;
