import React ,{useLayoutEffect ,useEffect,useRef}from 'react'

function UseLayoutEffect() {
    const inputRef = useRef(null) ;
    //useLayoutEffect render plus fast 
    useLayoutEffect(() => {
        console.log(inputRef.current.value);
    },[]);
    useEffect(()=>{
       inputRef.current.value = "Hello"
    },[]) ;
  return (
    <div>
       <input ref={inputRef} value="PEDRO" style={{ width: 400, height: 60 }}/>
    </div>
  )
}

export default UseLayoutEffect