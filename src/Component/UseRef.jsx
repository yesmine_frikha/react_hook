import React, {useRef} from 'react'

function UseRef() {
    //document.getElementbyId  dont need to do that because reactDom it's virtual 
    const inputRef = useRef(null)
    const onClick =()=> {
        // console.log(inputRef.current.value)
        // inputRef.current.focus();
        inputRef.current.value= ""
    }
    return (
    <div>
        <h1>Pedro</h1>
        <input type="text" palaachoder="Ex.." ref={inputRef}/>
        <button onClick={onClick}>Change Name</button>
    </div>
  )
}

export default UseRef